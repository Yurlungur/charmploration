#include <stdio.h>
#include "MyModule.decl.h"

/*
  Good God. This is a "simple" example in the charm++
  book... basically the second program they show you. And yet it's
  already horrendously complicated. Already I have trouble following
  the logic of the code... which I wrote!

  If I want to follow the logic of the program, I have to jump from
  the main class to the simple class to the main class again. And I
  have to keep track of a private variable "count."

  WHAT?

  No one on Earth would call this good program design. It's designed
  to be completely unreadable.
 */

/* mainchare */
class Main : public CBase_Main {
private:
  int count = 10; // wait for 10 responses
  double pi = 3.1415;
public:
  Main(CkArgMsg* m) {
    ckout << "Hello world!" << endl;
    CProxy_Simple sim = CProxy_Simple::ckNew(pi,thisProxy);
    for (int i = 0; i < 10; i++) {
      sim.findArea(i,0);
    }
  };

  void printArea(int r, double area) {
    ckout << "Area of a circle of radius "
	  << r << " is " << area
	  << endl;
    count--;
    if (count == 0) CkExit();
  }
};

class Simple : public CBase_Simple {
private:
  float y;
  CProxy_Main mainProxy;
public:
  Simple( double pi, CProxy_Main master) {
    y = pi;
    mainProxy = master;
    ckout << "Hello a simple chare running on  "
	  << CkMyPe()
	  << endl;
  }
  void findArea(int r, bool done) {
    mainProxy.printArea(r,y*r*r);
  }
};

#include "MyModule.def.h"
