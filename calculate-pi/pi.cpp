#include <stdio.h>
#include "MyModule.decl.h"

CProxy_Master mainProxy; //readonly

/* mainchare */
class Master : public CBase_Master {
private:
  int count, totalInsideCircle, totalNumTrials;
  double tstart,tend; // for profiling
public:
  Master(CkArgMsg* m) {
    if (m -> argc < 3) {
      ckout << "Need numTrials as a command line parameter"
	    << endl;
      CkExit();
    }
    int numTrials = atoi(m->argv[1]);
    int numChares = atoi(m->argv[2]);
    tstart = CkWallTimer(); // CkWallTimer() returns seconds since
			    // start of program
    if ( numTrials % numChares) {
      ckout << "Number of trials must be divisible by number of chares."
	    << endl;
      CkExit();
    }
    for (int i = 0; i < numChares; i++) {
      CProxy_Worker::ckNew(numTrials / numChares);
    }
    count = numChares; // wait for count responses
    mainProxy = thisProxy;
    totalInsideCircle = totalNumTrials = 0;
  }

  void addContribution(int numIn, int numTrials) {
    totalInsideCircle += numIn;
    totalNumTrials += numTrials;
    count--;
    if (count == 0) {
      double myPi = 4.0*((double) totalInsideCircle)/((double) totalNumTrials);
      ckout << "Approximate value of pi is: " << myPi << endl;
      tend = CkWallTimer();
      ckout << "Time spent on calculation is: " << tend - tstart
	    << " seconds." << endl;
      CkExit();
    }
  }
};

class Worker : public CBase_Worker {
private:
  int inTheCircle = 0;
  double x,y;
public:
  Worker(int numTrials) {
    ckout << "Hello from a simple chare running on "
	  << CkMyPe()
	  << endl;
    for (int i = 0; i < numTrials; i++) {
      x = drand48();
      y = drand48();
      if ( (x*x + y*y) < 1.0 ) inTheCircle++;
    }
    mainProxy.addContribution(inTheCircle,numTrials);
  }
};

#include "MyModule.def.h"
