#include "fib.decl.h"

const int THRESHOLD = 10;

class Main : public CBase_Main {
public:
  Main (CkArgMsg* m) {
    CkFuture fmain = CkCreateFuture();
    CProxy_Fib::ckNew(atoi(m->argv[1]),true,fmain);
  }
};

/*
  Futures must pass charm++ messages. See:
  http://charm.cs.illinois.edu/manuals/html/charm++/10.html
 */
class ValueMsg : public CMessage_ValueMsg {
public:
  int value;
  ValueMsg() { value = 0; }
  ValueMsg(int v) { value = v; }
};

class Fib : public CBase_Fib {
public:
  Fib(int n, bool isRoot, CkFuture f) {
    run(n,isRoot,f);
  }
  int seqFib(int n) { return n < 2 ? n : seqFib(n - 1) + seqFib(n - 2); }
  void run(int n, bool isRoot, CkFuture f) {
    int result;
    if (n < THRESHOLD) result = seqFib(n);
    else {
      CkFuture f1 = CkCreateFuture();
      CkFuture f2 = CkCreateFuture();
      CProxy_Fib::ckNew(n-1,false,f1);
      CProxy_Fib::ckNew(n-2,false,f2);
      ValueMsg * m1 = (ValueMsg *) CkWaitFuture(f1);
      ValueMsg * m2 = (ValueMsg *) CkWaitFuture(f2);
      result = m1->value + m2->value;
      delete m1; delete m2;
    }
    if (isRoot) {
      CkPrintf("The requested Fibonacci number is: %d\n", result);
      CkExit();
    } else {
      ValueMsg *m = new ValueMsg(result);
      CkSendToFuture(f,m);
    }
  }
};

#include "fib.def.h"
