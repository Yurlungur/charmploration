#include "fib.decl.h"

const int THRESHOLD = 10;

class Main : public CBase_Main {
public:
  Main(CkArgMsg* m) {
    CProxy_Fib::ckNew(atoi(m->argv[1]), true, CProxy_Fib()); 
  }
};

class Fib : public CBase_Fib {
public:
  // this is also terrible. I have to include an UNKOWN
  // #DEFINE variable at the top of my code
  // which is auto-generated code that I am told I don't need
  // to understand. I'm fine with auto-generated code, but this
  // is MESSY and I don't like it.
  Fib_SDAG_CODE
  CProxy_Fib parent; bool isRoot;

  Fib(int n, bool isRoot_, CProxy_Fib parent_)
    : parent(parent_)
    , isRoot(isRoot_) {
    calc(n);
  }

  int seqFib(int n) {
    return (n < 2) ? n : seqFib(n-1) + seqFib(n - 2); 
  }

  void respond(int val) {
    if (!isRoot) {
      parent.response(val);
      delete this;
    } else {
      CkPrintf("Fibonacci number is %d\n", val);
      CkExit();
    }
  }
};

#include "fib.def.h"
