#include <stdio.h>
#include <math.h>
#include "MyModule.decl.h"

/* mainchare */
class Main : public CBase_Main {
public:
  Main(CkArgMsg* m) {
    ckout << "Hello world!" << endl;
    if (m->argc > 1) {
      // endl provided by charm++? Not std::endl?
      ckout << "...And hello "
	    << m -> argv[1] << "!!!" << endl;
    }
    double pi = M_PI;
    CProxy_Simple::ckNew(12,pi);
  };
};

class Simple : public CBase_Simple {
public:
  Simple(int x, double y) {
    ckout << "Hello from a simple chare running on "
	  << CkMyPe()
	  << endl;
    ckout << "Area of a circle of radius " << x << " is "
	  << y*x*x
	  << endl;
    CProxy_End::ckNew();
  }
};

class End : public CBase_End {
public:
  End() {
    ckout << "Ending program now." << endl;
    CkExit();
  }
};

#include "MyModule.def.h"
