#include "multi_ring.hpp"

int Ring::nextI(int s) {
  return ((thisIndex + s) % ringSize);
}

void Ring::doSomething(int elementsLeft,
		       int tripsLeft,
		       int fromIndex,
		       int fromPE) {
  int skipAmount;

  // Display some text to the user
  printf("Ring_%d[%d](%d): tripsLeft = %d, from [%d](%d)\n",
	 ringID,thisIndex,CkMyPe(),tripsLeft,fromIndex,fromPE);

  // Send message to continue traversals or notify main
  if (elementsLeft > 1) {// elements left in traversal
    skipAmount = (rand() % elementsLeft) + 1;
    thisProxy(nextI(skipAmount)).doSomething(elementsLeft - skipAmount,
					     tripsLeft,
					     thisIndex,
					     CkMyPe());
  } else if (tripsLeft > 1) {
    thisProxy(nextI(1)).doSomething( ringSize,
				     tripsLeft - 1,
				     thisIndex,
				     CkMyPe());
  } else {
    mainProxy.ringFinished();
  }
}

#include "multi_ring.def.h"
