#pragma once
#include "multi_ring_main.hpp"
#include "multi_ring.decl.h"

class Ring : public CBase_Ring {
private:
  CProxy_Main mainProxy; // Proxy for the main chare
  int ringSize; // number of elements in the ring
  int ringID; // ID value of this ring

public:
  Ring(CkMigrateMessage *msg) {}
  Ring(CProxy_Main mp, int rs, int rID)
    : mainProxy(mp)
    , ringSize(rs)
    , ringID(rID) {}

  int nextI(int s);

  void doSomething(int elementsLeft, int tripsLeft, int fromIndex, int fromPE);
};
