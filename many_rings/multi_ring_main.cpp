#include "multi_ring_main.hpp"
#include "multi_ring.hpp"

Main::Main(CkArgMsg* msg) {
  CkPrintf("\"Array Ring(Multi)\" Program\n");
  processCommandLine(msg);
  CkPrintf(" numRings = %d, #Pes() = %d\n",
	   numRings,CkNumPes());
  for (int i = 0; i < numRings; i++) {
    CkPrintf(" Ring_%d : ringSize = %d, tripCount = %d\n",
	     i, ringSize[i], tripCount[i]);
  }
  // Create the rings
  CkPrintf("Creating rings...\n");
  for (int i = 0; i < numRings; i++) {
    CProxy_Ring ring = CProxy_Ring::ckNew(thisProxy,
					  ringSize[i],
					  i,
					  ringSize[i]);
    ring(rand() % ringSize[i]).doSomething(ringSize[i],tripCount[i],-1,-1);
  }
}

void Main::processCommandLine(CkArgMsg* msg) {
  CkPrintf("Reading in data...\n");
  numRings = atoi(msg -> argv[1]);
  CkPrintf("Resizing vectors...\n");
  ringSize.resize(numRings);
  tripCount.resize(numRings);
  CkPrintf("Filling vectors...\n");
  for (int i = 0; i < numRings; i++) {
    ringSize[i] = atoi(msg -> argv[i+1+1]);
    tripCount[i] = atoi(msg -> argv[i+1+1+numRings]);
  }
}

void  Main::ringFinished(){
  if ((++finishedCount) >= numRings) {
    CkExit();
  }
}

#include "multi_ring_main.def.h"
