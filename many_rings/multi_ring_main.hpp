#pragma once
#include "multi_ring_main.decl.h"
#include <vector>

class Main : public CBase_Main {
private:
  int numRings;
  std::vector<int> ringSize;
  std::vector<int> tripCount;
  int finishedCount = 0;

public:
  Main(CkMigrateMessage *msg) {}
  Main(CkArgMsg* msg);
  
  void processCommandLine(CkArgMsg* msg);

  void ringFinished();
};


