#include <stdio.h>
#include "MyModule.decl.h"

CProxy_Master mainProxy; // readonly

/* mainchare */
class Master : public CBase_Master {
private:
  int count = 10; // seriously. f**k this book-keeping.
  double pi = 3.1415;
public:
  Master(CkArgMsg* m) {
    for (int i = 0; i < 10; i++) {
      CProxy_Worker::ckNew(i,pi);
    }
    mainProxy=thisProxy;
  }

  void printArea(int r, double area) {
    ckout << "Area of a circle of radius " << r << " is "
	  << area << endl;
    count--;
    if (count == 0) CkExit();
  }
};

class Worker : public CBase_Worker {
private:
  float y;
public:
  Worker (int r, double pi ) {
    y = pi;
    ckout << "Hello from a simple chare running on "
	  << CkMyPe() << endl;
    mainProxy.printArea(r,y*r*r);
  }
};

#include "MyModule.def.h"
