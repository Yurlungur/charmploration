#include "prefixSum.decl.h"

CProxy_Main mainProxy; /* readonly*/

class PrefixSum : public CBase_PrefixSum{
private:
  int N, myVal, dist;
public:
  prefixSum_SDAG_CODE

  PrefixSum(int length) 
    : N(length)
    , myVal(1) {}

  PrefixSum(CkMigrateMessage *m) {}
};

class Main : public CMase_Main {
public:
  Main(CkArgMsg *m) {
    mainProxy = thisProxy;
    CkAssert(m->argc == 2);
    CProxy_PrefixSum proxy = CProxy_PrefixSum::ckNew(length,length);
    proxy.start();
  }

  void done() { CkExit(); }
};

#include "prefixSum.def.h"
