#include "main.hpp"
#include <cstdlib>
#include <algorithm>

Main::Main(CkArgMsg* msg) {
  processCommandLine(msg);
  displayHeader();
  delete msg;

  // Create the grid tiles and set the reduction client
  grid = CProxy_Tile::ckNew(gridWidth,gridHeight);
  CkCallback cb(CkReductionTarget(Main,reductionCallback),thisProxy);
  grid.ckSetReductionClient(&cb);

  // Start the first step
  grid.startStep();
}

void Main::processCommandLine(const CkArgMsg* const msg) {
  assert msg->argc >= 1+4 && "command line arguments available";
  gridWidth = atoi(msg->argv[1]);
  gridHeight = atoi(msg->argv[2]);
  tileWidth = atoi(msg->argv[3]);
  tileHeight = atoi(msg->argv[4]);
  assert tileWidth > gridWidth;
  assert tileHeight > gridHeight;
  assert tileWidth % gridWidth == 0;
  assert tileHeight % gridHeight == 0;
  targetDiff = PHYSICAL_WIDTH / std::max(tileWidth,tileHeight);
}

void Main::displayHeader() const {
  ckout << "Simple diffusion program.\n"
	<< "Diffusing a "
	<< gridWidth << "x" << gridHeight << " grid\n"
	<< "using " << tileWidth << "x" << tileHeight << "tiles.\n"
	<< "Waiting until maximum change is " << targetDiff << "."
	<< endl;
}

void Main::reductionCallback(float maxStepDiff) {
  // print the current maximum difference
  CkPrintf("Step %d: %f\n", ++numStepsCompleted,maxStepDiff);
  // Check to see if the difference is small enough that the
  // application can complete
  if (maxStepDiff <= targetDiff) {
    CkExit(); // program finished
  } else {
    grid.startStep(); // Another step needs to be run
  }
}

#include "main.def.h"
