#include "main.decl.h"

const float PHYSICAL_WIDTH = 1.0

class Main : public CBase_Main {
private:
  int numStepsCompleted = 0;
  CProxy_Tile grid;
  void processCommandLine(const CkArgMsg* const msg);
  void displayHeader() const;

public:
  Main(CkMigrateMessage* msg);
  Main(CkArgMsg* msg);
  void reductionCallback(float maxStepDiff);
};
