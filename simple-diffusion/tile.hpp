#include "tile.decl.h"
#include <vector>

class Tile : public CBase_Tile {
public:
  Tile();
  Tile(CkArgMsg* msg);
  void startStep();
  void recvNorthGhost(float* ghostData, int dataLen);
  void recvSouthGhost(float* ghostData, int dataLen);
  void recvWestGhost(float* ghostData, int dataLen);
  void recvEastGhost(float* ghostData, int dataLen);

private:
  std::vector<float> tileData;
  float maxDiff;
  void enforceConstants();
  void countEvent();
  void doCalc();
  int XY_TO_I(int x, int y) const;
};
