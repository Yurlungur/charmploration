#include "reduction.decl.h"
#include <cstdlib>

const int DEFAULT_NUM_ELEMENTS = 10;

class Main : public CBase_Main {
private:
  int numElements;
public:
  Main(CkArgMsg* msg) {
    numElements = msg->argc > 1 ? atoi(msg->argv[1]) : DEFAULT_NUM_ELEMENTS;
    CkPrintf("Reduction: number of elements = %d\n", numElements);
    CkPrintf("n(n-1)/2 = %d\n", numElements*(numElements-1)/2);
    CProxy_Elem::ckNew(thisProxy,numElements);
  }

  void printResult(int result) {
    CkPrintf("result = %d\n", result);
    CkExit();
  }
};

class Elem : public CBase_Elem {
public:
  Elem(CProxy_Main mainProxy) {
    int value = thisIndex;
    CkCallback cb(CkReductionTarget(Main,printResult), mainProxy);
    // seriously? We're not past this byte-level nonsense?
    contribute(sizeof(int), &value, CkReduction::sum_int, cb);
  }
  Elem(CkMigrateMessage*) {}
};

#include "reduction.def.h"
